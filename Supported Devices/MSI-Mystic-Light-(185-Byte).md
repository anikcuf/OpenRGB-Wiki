# MSI Mystic Light (185 Byte)

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1462` | `1720` | MSI Mystic Light MS_1720 |
| `1462` | `7B12` | MSI Mystic Light MS_7B12 |
| `1462` | `7B16` | MSI Mystic Light MS_7B16 |
| `1462` | `7B17` | MSI Mystic Light MS_7B17 |
| `1462` | `7B18` | MSI Mystic Light MS_7B18 |
| `1462` | `7B50` | MSI Mystic Light MS_7B50 |
| `1462` | `7B85` | MSI Mystic Light MS_7B85 |
| `1462` | `7B93` | MSI Mystic Light MS_7B93 |
| `1462` | `7C34` | MSI Mystic Light MS_7C34 |
| `1462` | `7C35` | MSI Mystic Light MS_7C35 |
| `1462` | `7C36` | MSI Mystic Light MS_7C36 |
| `1462` | `7C37` | MSI Mystic Light MS_7C37 |
| `1462` | `7C56` | MSI Mystic Light MS_7C56 |
| `1462` | `7C59` | MSI Mystic Light MS_7C59 |
| `1462` | `7C60` | MSI Mystic Light MS_7C60 |
| `1462` | `7C67` | MSI Mystic Light MS_7C67 |
| `1462` | `7C71` | MSI Mystic Light MS_7C71 |
| `1462` | `7C73` | MSI Mystic Light MS_7C73 |
| `1462` | `7C75` | MSI Mystic Light MS_7C75 |
| `1462` | `7C76` | MSI Mystic Light MS_7C76 |
| `1462` | `7C77` | MSI Mystic Light MS_7C77 |
| `1462` | `7C79` | MSI Mystic Light MS_7C79 |
| `1462` | `7C80` | MSI Mystic Light MS_7C80 |
| `1462` | `7C81` | MSI Mystic Light MS_7C81 |
| `1462` | `7C83` | MSI Mystic Light MS_7C83 |
| `1462` | `7C84` | MSI Mystic Light MS_7C84 |
| `1462` | `7C86` | MSI Mystic Light MS_7C86 |
| `1462` | `7C90` | MSI Mystic Light MS_7C90 |
| `1462` | `7C91` | MSI Mystic Light MS_7C91 |
| `1462` | `7C92` | MSI Mystic Light MS_7C92 |
| `1462` | `7C94` | MSI Mystic Light MS_7C94 |
| `1462` | `7C95` | MSI Mystic Light MS_7C95 |
| `1462` | `7C98` | MSI Mystic Light MS_7C98 |
| `1462` | `7D06` | MSI Mystic Light MS_7D06 |
| `1462` | `7D07` | MSI Mystic Light MS_7D07 |
| `1462` | `7D08` | MSI Mystic Light MS_7D08 |
| `1462` | `7D09` | MSI Mystic Light MS_7D09 |
| `1462` | `7D13` | MSI Mystic Light MS_7D13 |
| `1462` | `7D15` | MSI Mystic Light MS_7D15 |
| `1462` | `7D17` | MSI Mystic Light MS_7D17 |
| `1462` | `7D18` | MSI Mystic Light MS_7D18 |
| `1462` | `7D19` | MSI Mystic Light MS_7D19 |
| `1462` | `7D20` | MSI Mystic Light MS_7D20 |
| `1462` | `7D25` | MSI Mystic Light MS_7D25 |
| `1462` | `7D27` | MSI Mystic Light MS_7D27 |
| `1462` | `7D28` | MSI Mystic Light MS_7D28 |
| `1462` | `7D29` | MSI Mystic Light MS_7D29 |
| `1462` | `7D30` | MSI Mystic Light MS_7D30 |
| `1462` | `7D31` | MSI Mystic Light MS_7D31 |
| `1462` | `7D32` | MSI Mystic Light MS_7D32 |
| `1462` | `7D36` | MSI Mystic Light MS_7D36 |
| `1462` | `7D38` | MSI Mystic Light MS_7D38 |
| `1462` | `7D41` | MSI Mystic Light MS_7D41 |
| `1462` | `7D42` | MSI Mystic Light MS_7D42 |
| `1462` | `7D43` | MSI Mystic Light MS_7D43 |
| `1462` | `7D50` | MSI Mystic Light MS_7D50 |
| `1462` | `7D51` | MSI Mystic Light MS_7D51 |
| `1462` | `7D52` | MSI Mystic Light MS_7D52 |
| `1462` | `7D53` | MSI Mystic Light MS_7D53 |
| `1462` | `7D54` | MSI Mystic Light MS_7D54 |
| `1462` | `7D59` | MSI Mystic Light MS_7D59 |
| `1462` | `7D69` | MSI Mystic Light MS_7D69 |
| `1462` | `7D91` | MSI Mystic Light MS_7D91 |
| `1462` | `7E06` | MSI Mystic Light MS_7E06 |
| `1462` | `7D77` | MSI Mystic Light MS_7D77 |
