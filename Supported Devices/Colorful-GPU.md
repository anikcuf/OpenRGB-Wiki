# Colorful GPU

 This card only supports direct mode

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:2504` | `7377:150A` | iGame GeForce RTX 3060 Ultra W OC 12G L-V |
| `10DE:2484` | `7377:140A` | iGame GeForce RTX 3070 Advanced OC-V |
| `10DE:2484` | `7377:1401` | iGame GeForce RTX 3070 Advanced OC-V |
| `10DE:2482` | `7377:1400` | iGame GeForce RTX 3070 Ti Advanced OC-V |
