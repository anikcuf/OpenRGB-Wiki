# HyperX Pulsefire Dart

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0951` | `16E1` | HyperX Pulsefire Dart (Wireless) |
| `0951` | `16E2` | HyperX Pulsefire Dart (Wired) |
