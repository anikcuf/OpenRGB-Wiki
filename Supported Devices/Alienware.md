# Alienware

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `187C` | `0550` | Dell G Series LED Controller |
