# EVGA RGB v3 GPU

 EVGA has not exposed a per LED control method yet so OpenRGB
is only able to set all LED's to a single color.

## Connection Type
 I2C

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:2486` | `3842:3665` | EVGA GeForce RTX 3060TI FTW3 Gaming |
| `10DE:2486` | `3842:3667` | EVGA GeForce RTX 3060TI FTW3 Ultra |
| `10DE:2489` | `3842:4667` | EVGA GeForce RTX 3060TI FTW3 Ultra LHR |
| `10DE:2484` | `3842:3751` | EVGA GeForce RTX 3070 Black Gaming |
| `10DE:2484` | `3842:3753` | EVGA GeForce RTX 3070 XC3 Gaming |
| `10DE:2484` | `3842:3755` | EVGA GeForce RTX 3070 XC3 Ultra |
| `10DE:2488` | `3842:4755` | EVGA GeForce RTX 3070 XC3 Ultra LHR |
| `10DE:2484` | `3842:3767` | EVGA GeForce RTX 3070 FTW3 Ultra |
| `10DE:2488` | `3842:4767` | EVGA GeForce RTX 3070 FTW3 Ultra LHR |
| `10DE:2482` | `3842:3783` | EVGA GeForce RTX 3070Ti XC3 Gaming |
| `10DE:2482` | `3842:3785` | EVGA GeForce RTX 3070Ti XC3 Ultra |
| `10DE:2482` | `3842:3485` | EVGA GeForce RTX 3070Ti XC3 Ultra v2 |
| `10DE:2482` | `3842:3797` | EVGA GeForce RTX 3070Ti FTW3 Ultra |
| `10DE:2482` | `3842:3497` | EVGA GeForce RTX 3070Ti FTW3 Ultra v2 |
| `10DE:2206` | `3842:3881` | EVGA GeForce RTX 3080 XC3 Black |
| `10DE:2216` | `3842:4881` | EVGA GeForce RTX 3080 XC3 Black LHR |
| `10DE:2206` | `3842:3883` | EVGA GeForce RTX 3080 XC3 Gaming |
| `10DE:2216` | `3842:4883` | EVGA GeForce RTX 3080 XC3 Gaming LHR |
| `10DE:2206` | `3842:3885` | EVGA GeForce RTX 3080 XC3 Ultra |
| `10DE:2216` | `3842:4885` | EVGA GeForce RTX 3080 XC3 Ultra LHR |
| `10DE:2206` | `3842:3888` | EVGA GeForce RTX 3080 XC3 Ultra Hybrid |
| `10DE:2216` | `3842:4888` | EVGA GeForce RTX 3080 XC3 Ultra Hybrid LHR |
| `10DE:2206` | `3842:3889` | EVGA GeForce RTX 3080 XC3 Ultra Hydro Copper |
| `10DE:2206` | `3842:3895` | EVGA GeForce RTX 3080 FTW3 Gaming |
| `10DE:2206` | `3842:3897` | EVGA GeForce RTX 3080 FTW3 Ultra |
| `10DE:2216` | `3842:4297` | EVGA GeForce RTX 3080 FTW3 Ultra v2 LHR |
| `10DE:220A` | `3842:4877` | EVGA GeForce RTX 3080 FTW3 Ultra 12GB |
| `10DE:2216` | `3842:4897` | EVGA GeForce RTX 3080 FTW3 Ultra LHR |
| `10DE:2206` | `3842:3898` | EVGA GeForce RTX 3080 FTW3 Ultra Hybrid |
| `10DE:2216` | `3842:4898` | EVGA GeForce RTX 3080 FTW3 Ultra Hybrid LHR |
| `10DE:220A` | `3842:4878` | EVGA GeForce RTX 3080 FTW3 Ultra Hybrid Gaming LHR |
| `10DE:2206` | `3842:3899` | EVGA GeForce RTX 3080 FTW3 Ultra Hydro Copper |
| `10DE:220A` | `3842:4879` | EVGA GeForce RTX 3080 FTW3 Ultra Hydro Copper 12G |
| `10DE:2208` | `3842:3953` | EVGA GeForce RTX 3080Ti XC3 Gaming |
| `10DE:2208` | `3842:3955` | EVGA GeForce RTX 3080Ti XC3 Ultra Gaming |
| `10DE:2208` | `3842:3958` | EVGA GeForce RTX 3080Ti XC3 Gaming Hybrid |
| `10DE:2208` | `3842:3959` | EVGA GeForce RTX 3080Ti XC3 Gaming Hydro Copper |
| `10DE:2208` | `3842:3967` | EVGA GeForce RTX 3080Ti FTW3 Ultra |
| `10DE:2208` | `3842:3968` | EVGA GeForce RTX 3080Ti FTW3 Ultra Hybrid |
| `10DE:2208` | `3842:3969` | EVGA GeForce RTX 3080Ti FTW3 Ultra Hydro Copper |
| `10DE:2204` | `3842:3971` | EVGA GeForce RTX 3090 XC3 Black |
| `10DE:2204` | `3842:3973` | EVGA GeForce RTX 3090 XC3 Gaming |
| `10DE:2204` | `3842:3975` | EVGA GeForce RTX 3090 XC3 Ultra |
| `10DE:2204` | `3842:3978` | EVGA GeForce RTX 3090 XC3 Ultra Hybrid |
| `10DE:2204` | `3842:3979` | EVGA GeForce RTX 3090 XC3 Ultra Hydro Copper |
| `10DE:2204` | `3842:3987` | EVGA GeForce RTX 3090 FTW3 Ultra |
| `10DE:2204` | `3842:3982` | EVGA GeForce RTX 3090 FTW3 Ultra v2 |
| `10DE:2204` | `3842:3387` | EVGA GeForce RTX 3090 FTW3 Ultra v3 |
| `10DE:2204` | `3842:3988` | EVGA GeForce RTX 3090 FTW3 Ultra Hybrid |
| `10DE:2204` | `3842:3989` | EVGA GeForce RTX 3090 FTW3 Ultra Hydro Copper |
| `10DE:2204` | `3842:3998` | EVGA GeForce RTX 3090 K\|NGP\|N Hybrid |
| `10DE:2204` | `3842:3999` | EVGA GeForce RTX 3090 K\|NGP\|N Hydro Copper |
| `10DE:2203` | `3842:4983` | EVGA GeForce RTX 3090Ti FTW3 Gaming |
| `10DE:2203` | `3842:4985` | EVGA GeForce RTX 3090Ti FTW3 Ultra Gaming |
