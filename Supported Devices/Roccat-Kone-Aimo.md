# Roccat Kone Aimo

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1E7D` | `2E27` | Roccat Kone Aimo |
| `1E7D` | `2E2C` | Roccat Kone Aimo 16K |
