# Asus Aura Strix Evolve

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `185B` | ASUS ROG Strix Evolve |
