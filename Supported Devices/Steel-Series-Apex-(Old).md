# Steel Series Apex (Old)

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1038` | `1202` | SteelSeries Apex (OG)/Apex Fnatic |
| `1038` | `1206` | SteelSeries Apex 350 |
