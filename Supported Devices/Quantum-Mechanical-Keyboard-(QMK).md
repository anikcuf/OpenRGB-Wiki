# Quantum Mechanical Keyboard (QMK)

 Please see [the github page](https://github.com/qmk/qmk_firmware#supported-keyboards) for the up to date list of
keyboards supported by the QMK controller.

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `` | `` |  |
