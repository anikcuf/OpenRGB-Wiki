# Lian Li Uni Hub

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is problematic (See device page for details)

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0CF2` | `0XA101` | Lian Li Uni Hub - AL |
