# Steel Series Rival 3

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1038` | `1710` | SteelSeries Rival 300 |
| `1038` | `1714` | Acer Predator Gaming Mouse (Rival 300) |
| `1038` | `1394` | SteelSeries Rival 300 CS:GO Fade Edition |
| `1038` | `1716` | SteelSeries Rival 300 CS:GO Fade Edition (stm32) |
| `1038` | `171a` | SteelSeries Rival 300 CS:GO Hyperbeast Edition |
| `1038` | `1392` | SteelSeries Rival 300 Dota 2 Edition |
| `1038` | `1718` | SteelSeries Rival 300 HP Omen Edition |
| `1038` | `1710` | SteelSeries Rival 300 Black Ops Edition |
| `1038` | `1824` | SteelSeries Rival 3 (Old Firmware) |
| `1038` | `184C` | SteelSeries Rival 3 |
