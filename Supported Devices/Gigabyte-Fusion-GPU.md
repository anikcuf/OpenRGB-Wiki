# Gigabyte Fusion GPU

 

## Connection Type
 I2C

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:1C82` | `1458:372A` | Gigabyte GTX1050 Ti G1 Gaming (rev A1) |
| `10DE:1C82` | `1458:372A` | Gigabyte GTX1050 Ti G1 Gaming |
| `10DE:1C03` | `1458:3716` | Gigabyte GTX1060 G1 Gaming 6G |
| `10DE:1C03` | `1458:3739` | Gigabyte GTX1060 G1 Gaming 6G OC |
| `10DE:1C03` | `1458:3757` | Gigabyte GTX1060 Xtreme Gaming V1 |
| `10DE:1C03` | `1458:3776` | Gigabyte GTX1060 Xtreme Gaming v2 |
| `10DE:1B81` | `1458:3778` | Gigabyte GTX1070 Xtreme Gaming |
| `10DE:1B81` | `1458:3772` | Gigabyte GTX1070 G1 Gaming 8G V1 |
| `10DE:1B82` | `1458:3794` | Gigabyte GTX1070 Ti 8G Gaming |
| `10DE:1B80` | `1458:3702` | Gigabyte GTX1080 G1 Gaming |
| `10DE:1B06` | `1458:3752` | Gigabyte GTX1080 Ti 11G |
| `10DE:1B06` | `1458:374C` | Gigabyte GTX1080 Ti Gaming OC 11G |
| `10DE:1B06` | `1458:377A` | Gigabyte GTX1080 Ti Gaming OC BLACK 11G |
| `10DE:1B06` | `1458:3751` | Gigabyte GTX1080 Ti Xtreme Edition |
| `10DE:1B06` | `1458:3762` | Gigabyte GTX1080 Ti Xtreme Waterforce Edition |
| `10DE:1F82` | `1458:3FE4` | Gigabyte GTX1650 Gaming OC |
| `10DE:2184` | `1458:3FC7` | Gigabyte GTX1660 Gaming OC 6G |
| `10DE:21C4` | `1458:4014` | Gigabyte GTX1660 SUPER Gaming OC |
| `10DE:1F08` | `1458:37CE` | Gigabyte RTX2060 Gaming OC |
| `10DE:1F08` | `1458:3FC2` | Gigabyte RTX2060 Gaming OC PRO |
| `10DE:1F08` | `1458:3FC9` | Gigabyte RTX2060 Gaming OC PRO V2 |
| `10DE:1F08` | `1458:3FD0` | Gigabyte RTX2060 Gaming OC PRO White |
| `10DE:1F06` | `1458:404A` | Gigabyte RTX2060 SUPER Gaming |
| `10DE:1F06` | `1458:3FED` | Gigabyte RTX2060 SUPER Gaming OC |
| `10DE:1F06` | `1458:3FFE` | Gigabyte RTX2060 SUPER Gaming OC 3X White 8G |
| `10DE:1F06` | `1458:4009` | Gigabyte RTX2060 SUPER Gaming OC 3X 8G V2 |
| `10DE:1F02` | `1458:37AD` | Gigabyte RTX2070 Gaming OC 8G |
| `10DE:1F02` | `1458:37C2` | Gigabyte RTX2070 Windforce 8G |
| `10DE:1E84` | `1458:3FEB` | Gigabyte RTX2070S Gaming OC |
| `10DE:1E84` | `1458:4008` | Gigabyte RTX2070S Gaming OC 3X |
| `10DE:1E84` | `1458:4008` | Gigabyte RTX2070S Gaming OC 3X |
| `10DE:1E84` | `1458:400D` | Gigabyte RTX2070S Gaming OC 3X White |
| `10DE:1E82` | `1458:37D6` | Gigabyte RTX2080 Gaming OC 8G |
| `10DE:1E87` | `1458:37A7` | Gigabyte RTX2080 Gaming OC 8G |
| `10DE:1E07` | `1458:37A9` | Gigabyte RTX2080 Ti GAMING OC 11G |
| `10DE:1E81` | `1458:3FE9` | Gigabyte RTX2080S Gaming OC 8G |
| `10DE:2507` | `1458:40AA` | Gigabyte RTX3050 Gaming OC 8G |
| `10DE:2503` | `1458:4072` | Gigabyte RTX3060 EAGLE OC 12G |
| `10DE:2504` | `1458:4072` | Gigabyte RTX3060 EAGLE OC 12G V2 |
| `10DE:2504` | `1458:4071` | Gigabyte RTX3060 EAGLE 12G LHR V2 |
| `10DE:2503` | `1458:4073` | Gigabyte RTX3060 Vision OC 12G |
| `10DE:2504` | `1458:4073` | Gigabyte RTX3060 Vision OC 12G LHR |
| `10DE:2487` | `1458:4073` | Gigabyte RTX3060 Vision OC 12G v3.0 |
| `10DE:2503` | `1458:4074` | Gigabyte RTX3060 Gaming OC 12G |
| `10DE:2487` | `1458:4074` | Gigabyte RTX3060 Gaming OC 12G (rev. 2.0) |
| `10DE:2504` | `1458:4074` | Gigabyte RTX3060 Gaming OC 12G |
| `10DE:2486` | `1458:405B` | Gigabyte RTX3060 Ti EAGLE OC 8G |
| `10DE:2489` | `1458:405B` | Gigabyte RTX3060 Ti EAGLE OC 8G V2.0 LHR |
| `10DE:2489` | `1458:4060` | Gigabyte RTX3060 Ti EAGLE OC 8G V2.0 LHR |
| `10DE:2489` | `1458:4077` | Gigabyte RTX3060 Ti Vision OC 8G |
| `10DE:2484` | `1458:404C` | Gigabyte RTX3070 Gaming OC 8G |
| `10DE:2488` | `1458:404C` | Gigabyte RTX3070 Gaming OC 8G v3.0 LHR |
| `10DE:2484` | `1458:404D` | Gigabyte RTX3070 Vision 8G |
| `10DE:2488` | `1458:404D` | Gigabyte RTX3070 Vision 8G V2.0 LHR |
| `10DE:2484` | `1458:404E` | Gigabyte RTX3070 Eagle OC 8G |
| `10DE:2482` | `1458:408F` | Gigabyte RTX3070 Ti Gaming OC 8G |
| `10DE:2482` | `1458:408C` | Gigabyte RTX3070 Ti EAGLE 8G |
| `10DE:2482` | `1458:4090` | Gigabyte RTX3070 Ti Vision OC 8G |
| `10DE:2206` | `1458:403F` | Gigabyte RTX3080 Gaming OC 10G |
| `10DE:220A` | `1458:40A2` | Gigabyte RTX3080 Gaming OC 12G |
| `10DE:2216` | `1458:403F` | Gigabyte RTX3080 Gaming OC 10G |
| `10DE:2216` | `1458:404B` | Gigabyte RTX3080 Vision OC 10G (REV 2.0) |
| `10DE:2206` | `1458:404B` | Gigabyte RTX3080 Vision OC 10G |
| `10DE:2208` | `1458:4088` | Gigabyte RTX3080 Ti Gaming OC 12G |
| `10DE:2208` | `1458:4085` | Gigabyte RTX3080 Ti EAGLE 12G |
| `10DE:2204` | `1458:4043` | Gigabyte RTX3090 Gaming OC 24G |
