# Redragon K556

USB HID, 64-byte packets.  Every packet should perform both an HID write operation and an HID read operation sequentially.  Do not use feature reports.

## **Packet Structure**

All packets seem to follow this structure.  The command field indicates which data bank to access and whether the command is a read or a write.  The read or write operation uses the data starting at 0x08 in the packet of the given number of bytes and reads/writes it at the given offset.

| Index | Value | Description        |
| ----- | ----- | ------------------ |
| 0x00  | 0x04  |                    |
| 0x01  | 0xCC  | Checksum LSB       |
| 0x02  | 0xCC  | Checksum MSB       |
| 0x03  | 0xXX  | Command            |
| 0x04  | 0xNN  | Number of bytes    |
| 0x05  | 0xNN  | Byte offset LSB    |
| 0x06  | 0xNN  | Byte offset MSB    |
| 0x07  | 0x00  |                    |
| 0x08+ | 0xXX  | Data bytes         |

The checksum simply adds together all of the bytes in the packet excluding the first three.

## **Commands**

| Value | R/W   | Description             | Total Data Length |
| ----- | ----- | ----------------------- | ----------------- |
| 0x00  |       |                         |                   |
| 0x01  |       | Begin                   | 0                 |
| 0x02  |       | End                     | 0                 |
| 0x03  | Read  | Unknown Data            | 0x2C              |
| 0x04  | Write | Unknown Data            | 0x2C              |
| 0x05  | Read  | Parameter Data          | 0x8C              |
| 0x06  | Write | Parameter Data          | 0x8C              |
| 0x07  |       |                         |                   |
| 0x08  |       |                         |                   |
| 0x09  |       |                         |                   |
| 0x0A  |       |                         |                   |
| 0x0B  |       |                         |                   |
| 0x0C  |       |                         |                   |
| 0x0D  |       |                         |                   |
| 0x0E  |       |                         |                   |
| 0x0F  |       |                         |                   |
| 0x10  | Read  | Custom Color Data       |                   |
| 0x11  | Write | Custom Color Data       |                   |

## **Unknown Data Block (0x03/0x04)**

This data block is updated when changing profiles.

The data starting at index 0x10 appears to be the list of available modes.  It matches the order of the modes that show in the UI.

| Byte Index | Bytes | Description |
| ---------- | ----- | ----------- |
| 0x00       | 1     | 0x55        |
| 0x01       | 1     | 0xAA        |
| 0x02       | 1     | 0xFF        |
| 0x03       | 1     | 0x02        |
| 0x04       | 1     | 0x45        |
| 0x05       | 1     | 0x0C        |
| 0x06       | 1     | 0x04        |
| 0x07       | 1     | 0x50        |
| 0x08       | 1     | 0x07        |
| 0x09       | 1     | 0x01        |
| 0x0A       | 1     | Active Profile (0x00-0x03) |
| 0x0B       | 1     | 0x18        |
| 0x0C       | 1     | 0x00        |
| 0x0D       | 1     | 0x00        |
| 0x0E       | 1     | 0x00        |
| 0x0F       | 1     | 0x00        |
| 0x10       | 1     | 0x01        |
| 0x11       | 1     | 0x02        |
| 0x12       | 1     | 0x03        |
| 0x13       | 1     | 0x04        |

## **Parameter Data Block (0x05/0x06)**

Parameter table starting at offset 0x00 is Profile 1's parameter configuration.  Table repeats at offset 0x2A (Profile 2) and 0x54 (Profile 3).

| Byte Index      | Parameter Bytes | Parameter Description          |
| --------------- | --------------- | ------------------------------ |
| 0x00            | 1               | Mode (See Modes table)         |
| 0x01            | 1               | Brightness (0-5)               |
| 0x02            | 1               | Speed (Slow: 5, Fast: 0)       |
| 0x03            | 1               | Direction (R: 0, L: 1)         |
| 0x04            | 1               | Random Color Flag (0/1)        |
| 0x05            | 1               | Mode Red Color                 |
| 0x06            | 1               | Mode Green Color               |
| 0x07            | 1               | Mode Blue Color                |
| 0x08            | 1               |                                |
| 0x09            | 1               | Background Red Color           |
| 0x0A            | 1               | Background Green Color         |
| 0x0B            | 1               | Background Blue Color          |
| 0x0C            | 1               |                                |
| 0x0D            | 1               |                                |
| 0x0E            | 1               |                                |
| 0x0F            | 1               | Polling Rate (See table)       |
| 0x10            | 1               |                                |
| 0x11            | 1               | Surmount mode color (see table)|

## Modes

The effects all have very strange names.  I'm pretty sure the people who wrote the app didn't speak very good English and used some form of translator and/or autocorrect to come up with some of these atrocities.

| Effect name            | Value | Description                                         |
| ---------------------- | ----- | --------------------------------------------------- |
| Go with the stream     | 0x01  | Rainbow Wave                                        |
| Clouds fly             | 0x02  | Rainbow Wave, but stretched out                     |
| Winding paths          | 0x03  | Color Wheel                                         |
| The trial of light     | 0x04  | Spectrum Cycle                                      |
| Breathing              | 0x05  | Breathing (at least they got this right)            |
| Normally on            | 0x06  | Static                                              |
| Pass without trace     | 0x07  | Reactive (single key lights when pressed)           |
| Ripple graff           | 0x08  | Reactive with ripple from pressed key               |
| Fast run without trace | 0x09  | Reactive with ripple only on key's row              |
| Swift action           | 0x0A  | I think Razer calls this Starlight                  |
| Flowers blooming       | 0x0B  | Spectrum Cycle per key, very colorful               |
| Snow winter jasmine    | 0x0C  | Rainbow Wave, but vertical                          |
| Hurricane              | 0x0D  | Zigzag lines from center of keyboard                |
| Accumulate             | 0x0E  | Colors come in from outer edges and group in middle |
| Digital times          | 0x0F  | A slower version of Swift action (starlight)        |
| Both ways              | 0x10  | Visor effect that scans back and forth              |
| Surmount               | 0x11  | Seems to fade between hues of selected color        |
| Fast and the Furious   | 0x12  | Circular rainbow wave that moves to center          |
| Coastal                | 0x14  | Per-key control (did they mean "Custom"?)           |

## Surmount Mode Colors

| Surmount Index | Surmount Color |
| -------------- | -------------- |
| 0x00           | Red            |
| 0x01           | Yellow         |
| 0x02           | Green          |
| 0x03           | Blue           |

## Polling Rates

| Polling Rate Index | Polling Rate Setting |
| ------------------ | -------------------- |
| 0x00               | 125Hz                |
| 0x01               | 250Hz                |
| 0x02               | 500Hz                |
| 0x03               | 1000Hz               |

### Unknown Read (0x07)

Sent when opening the app.  Multi-packet read of memory region with total size 0x046E.  Perhaps this is the key map?  21 packets total.  Three of them have size 0x2A per packet (6 0x38 size followed by 1 0x2A size, repeat 3 times).

### Unknown Command (0x08)

Sent when changing a key binding.  Seems to be a multi-packet data stream with size 0x38 per packet.  Perhaps this is the key map?  21 packets total.  Three of them have size 0x2A per packet (6 0x38 size followed by 1 0x2A size, repeat 3 times).

### Unknown Read (0x09)

Sent when opening the app.  Number of bytes 0x10.

### Unknown Command (0x0A)

Sent when changing a key binding

### Unknown Read Data Command (0x0F)

Sent when opening the app.  Seems to be a multi-packet data stream with size 0x38 per packet.  7 packets total, with the last packet having a size of 0x2A instead of 0x38.  All data bytes are zero.  I think this is reading out color data.

### Read Custom Color Data (0x10)

### Write Custom Color Data (0x11)

## **Custom Color Keymap**

Each color data index represents 3 bytes in the color data buffer, RGB format

| Color data index | Key          |
| ---------------- | ------------ |
| 0x00             | Escape       |
| 0x01             | F1           |
| 0x02             | F2           |
| 0x03             | F3           |
| 0x04             | F4           |
| 0x05             | F5           |
| 0x06             | F6           |
| 0x07             | F7           |
| 0x08             | F8           |
| 0x09             | F9           |
| 0x0A             | F10          |
| 0x0B             | F11          |
| 0x0C             | F12          |
| 0x0D             |              |
| 0x0E             | Print Screen |
| 0x0F             | Scroll Lock  |
| 0x10             | Pause/Break  |
| 0x11             |              |
| 0x12             |              |
| 0x13             |              |
| 0x14             |              |
| 0x15             | `/~          |
| 0x16             | 1            |
| 0x17             | 2            |
| 0x18             | 3            |
| 0x19             | 4            |
| 0x1A             | 5            |
| 0x1B             | 6            |
| 0x1C             | 7            |
| 0x1D             | 8            |
| 0x1E             | 9            |
| 0x1F             | 0            |
| 0x20             | -/_          |
| 0x21             | =/+          |
| 0x22             | Backspace    |
| 0x23             | Insert       |
| 0x24             | Home         |
| 0x25             | Page Up      |
| 0x26             | Num Lock     |
| 0x27             | Num Pad /    |
| 0x28             | Num Pad *    |
| 0x29             | Num Pad -    |
| 0x2A             | Tab          |
| 0x2B             | Q            |
| 0x2C             | W            |
| 0x2D             | E            |
| 0x2E             | R            |
| 0x2F             | T            |
| 0x30             | Y            |
| 0x31             | U            |
| 0x32             | I            |
| 0x33             | O            |
| 0x34             | P            |
| 0x35             | [/{          |
| 0x36             | ]/}          |
| 0x37             | \/|          |
| 0x38             | Delete       |
| 0x39             | End          |
| 0x3A             | Page Down    |
| 0x3B             | Num Pad 7    |
| 0x3C             | Num Pad 8    |
| 0x3D             | Num Pad 9    |
| 0x3E             | Num Pad +    |
| 0x3F             | Caps Lock    |
| 0x40             | A            |
| 0x41             | S            |
| 0x42             | D            |
| 0x43             | F            |
| 0x44             | G            |
| 0x45             | H            |
| 0x46             | J            |
| 0x47             | K            |
| 0x48             | L            |
| 0x49             | ;/:          |
| 0x4A             | '/"          |
| 0x4B             |              |
| 0x4C             | Enter        |
| 0x4D             |              |
| 0x4E             |              |
| 0x4F             |              |
| 0x50             | Num Pad 4    |
| 0x51             | Num Pad 5    |
| 0x52             | Num Pad 6    |
| 0x53             |              |
| 0x54             | Left Shift   |
| 0x55             |              |
| 0x56             | Z            |
| 0x57             | X            |
| 0x58             | C            |
| 0x59             | V            |
| 0x5A             | B            |
| 0x5B             | N            |
| 0x5C             | M            |
| 0x5D             | ,/<          |
| 0x5E             | ./>          |
| 0x5F             | /?           |
| 0x60             |              |
| 0x61             | Right Shift  |
| 0x62             |              |
| 0x63             | Up Arrow     |
| 0x64             |              |
| 0x65             | Num Pad 1    |
| 0x66             | Num Pad 2    |
| 0x67             | Num Pad 3    |
| 0x68             | Num Pad Enter |
| 0x69             | Left Control |
| 0x6A             | Windows Key  |
| 0x6B             | Left Alt     |
| 0x6C             | Space Bar    |
| 0x6D             | Right Alt    |
| 0x6E             | Function     |
| 0x6F             | Context      |
| 0x70             |              |
| 0x71             | Right Control |
| 0x72             |              |
| 0x73             |              |
| 0x74             |              |
| 0x75             |              |
| 0x76             |              |
| 0x77             | Left Arrow   |
| 0x78             | Down Arrow   |
| 0x79             | Right Arrow  |
| 0x7A             |              |
| 0x7B             | Num Pad 0    |
| 0x7C             | Num Pad .    |
| 0x7D             |              |
